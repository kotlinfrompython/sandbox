package com.example.composeankomix

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.Composable
import androidx.compose.Recomposer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.ui.core.setContent
import androidx.ui.foundation.Text
import androidx.ui.layout.Column
import androidx.ui.layout.Row
import androidx.ui.tooling.preview.Preview
import com.example.composeankomix.ui.ComposeAnkoMixTheme
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainView().setContentView(this)
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = name)
}

@Composable
fun List(text: String) {
    Column {
        Row {
            Text(text = text)
        }
        Row {
            Text(text = "$text again")
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ComposeAnkoMixTheme {
        List("Compose Preview List")
    }
}

class MainView : AnkoComponent<MainActivity> {
    override fun createView(ui: AnkoContext<MainActivity>): View = ui.run {
        linearLayout {
            orientation = LinearLayout.VERTICAL
            textView("Anko")

            //At this time there only seems to be one way to call. This is because they have added
            //a function to ViewGroup which allows passing in a composable function. This allows
            //composable functions to be added within layouts such as the LinearLayout here.
            setContent(Recomposer.current()) {
                Greeting(name = "Compose from Anko")
            }
            textView("More Anko")
            setContent(Recomposer.current()) {
                List("More Compose from Anko")
            }
            recyclerView {
                layoutManager = LinearLayoutManager(ctx)
                adapter = MigrationAdapter()
                layoutParams = LinearLayout.LayoutParams(matchParent, wrapContent)
            }
        }
    }
}

class MigrationAdapter : RecyclerView.Adapter<MigrationAdapter.ViewHolder>() {
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val oldText: TextView = view.findViewById(R.id.old_xml_text)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.old_xml, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = 2

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (position) {
            0 -> holder.oldText.text = "Old Xml TextView In Recycler Position $position"
            1 -> holder.oldText.text = "Old Xml TextView In Recycler Position $position"
        }
        (holder.itemView as ViewGroup).setContent(Recomposer.current()) {
            Greeting(name = "Compose TextView in Recycler Adapter with Xml At Position $position")
        }
    }
}